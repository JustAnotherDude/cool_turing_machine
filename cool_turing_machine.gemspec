# frozen_string_literal: true

require_relative "lib/cool_turing_machine/version"

Gem::Specification.new do |spec|
  spec.name          = "CoolTuringMachine"
  spec.version       = CoolTuringMachine::VERSION
  spec.authors       = ["dude"]
  spec.email         = ["vanyaz158@gmail.com"]

  spec.summary       = "The simplest DSL that helps describe Turing's machine"
  spec.homepage      = "https://gitlab.com/JustAnotherDude/cool_turing_machine"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

    spec.metadata["homepage_uri"] = spec.homepage
    # spec.metadata["source_code_uri"] = "TODO: Put your gem's public repo URL here."
    # spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.required_ruby_version = ">= 2.6"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "zeitwerk", "~> 2.2"

  spec.add_development_dependency "bundler", ">= 2.0"
  spec.add_development_dependency "bundler-audit", "~>0.6"
  spec.add_development_dependency "deep-cover", ">= 0.7"
  spec.add_development_dependency "pry", ">= 0.12"
  spec.add_development_dependency "pry-byebug", ">= 3.7"
  spec.add_development_dependency "rake", ">= 12.0"
  spec.add_development_dependency "rspec", ">= 3.8"
  spec.add_development_dependency "rubocop-config-umbrellio", ">= 0.74"
  spec.add_development_dependency "yard", ">=0.9.20"
  spec.add_development_dependency "yard-junk", ">=0.0.7"
end
