# README

## Common Description

Gem provides the simplest DSL, which allows you to describe the Turing Machine.
Obviously, this gem is written for the sake of a fan, but if you want to use
the functionality of this wonderful gem, you should execute the following commands
in the terminal:

```bash
git clone git@gitlab.com:VanyaZ158/cool_turing_machine.git
cd cool_turing_machine
bundle
rake install # have fun?
```

## How to use

In order to describe Turing's machine, we need to declare the alphabet
with which this machine will work, as well as all its possible states,
from which we need to distinguish two special ones: the initial and the final.
An example of the simplest machine without defining transitions:

```ruby
machine = CoolTuringMachine.create do
  define_alphabet "abc"
  define_states :q0, :q1

  define_initial_state :q0
  define_final_state :q1
end
```

Here we declare a Turing machine, which takes the ribbon to the input,
which uses an alphabet consisting of three letters: *a, b, c*.
The machine can only be in two states: *q0* and *q1*, where q0 is the initial
state and q1 is the final state.
If we pass any tape to this machine now, we will get an error.

So let's declare several transitions for this machine:

```ruby
# Define transitions outside create block.
q0.match("a").change_char_to("c").move(:right).change_state_to(:q0)
q0.match("b").change_char_to("c").move(:right).change_state_to(:q0)
q0.match("c").change_char_to("c").move(:nowhere).change_state_to(:q1)
```

Thus, we will get a Machine that changes all the ribbon symbols to *c* until
the *c* symbol is found on the ribbon. After that, the machine finishes its
execution or, in other words, goes to the final state.

In order to execute the created machine, you need to call the #read_tape! method,
which takes one single `String` argument - the input tape.

```ruby
machine.read_tape!("abbbca") #=> "ccccca"
```
