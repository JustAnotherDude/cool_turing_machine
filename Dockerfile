FROM ruby:2.6-alpine

RUN apk add -U --no-cache git openssl build-base libxml2-dev libxslt-dev tzdata && \
  gem update --system && gem install bundler && bundle config --global jobs 10

WORKDIR  /app
