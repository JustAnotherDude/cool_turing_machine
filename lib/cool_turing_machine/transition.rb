# frozen_string_literal: true

module CoolTuringMachine
  # Represents transition between two states
  # @api public
  class Transition < BaseObject
    SIDE_ENUM = {
      right: 1,
      nowhere: 0,
      left: -1,
    }.freeze

    attr_accessor :machine

    attr_reader :state, :matching_char, :changing_char, :move_cursor_value, :next_state

    def initialize(state_name, machine)
      self.machine = machine

      @state = state_name
      @matching_char = nil
      @changing_char = nil
      @move_cursor_value = nil
      @next_state = nil
    end

    def match(alphabet_char)
      check_char_in_alphabet!(alphabet_char)

      @matching_char = alphabet_char
      tap { persist_transition! }
    end

    def change_char_to(alphabet_char)
      check_char_in_alphabet!(alphabet_char)

      tap { @changing_char = alphabet_char }
    end

    def move(side)
      tap { @move_cursor_value = SIDE_ENUM[side.to_sym] or error!("Bad side") }
    end

    def stay_same_state
      change_state_to(state)
    end

    def change_state_to(next_state)
      next_state = next_state.to_sym
      error!("State not found") unless state_names.include?(next_state)

      tap { @next_state = next_state }
    end

    def valid?
      [matching_char, changing_char, move_cursor_value, next_state].all?
    end

    def ==(other)
      return false unless other.is_a?(self.class)

      %i[state matching_char].all? { |field| other.public_send(field) == public_send(field) }
    end

    alias eql? ==

    def hash
      state.hash + matching_char.hash
    end

    private

    def alphabet
      machine.alphabet
    end

    def state_names
      machine.states.map(&:name)
    end

    def check_char_in_alphabet!(char)
      error!("Alphabet hasn't '#{char}' char") unless alphabet.include?(char)
    end

    def persist_transition!
      machine.transitions.delete(self).add(self)
    end
  end
end
