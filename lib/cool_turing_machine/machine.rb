# frozen_string_literal: true

module CoolTuringMachine
  # Describes behavior of Turing Machine.
  # Use {CoolTuringMachine.create} to initialize new {Machine}.
  # @!attribute alphabet
  #   @return [Array<String>] an array of alphabet symbols.
  # @!attribute states
  #   @return [Array<State>] list of states.
  # @!attribute transitions
  #   @return [Set<Transition>] unique list of transitions.
  # @!attribute initial_state
  #   @return [State] initial state of machine.
  # @!attribute final_state
  #   @return [State] final state of machine.
  class Machine < BaseObject
    # Simple object, which represents structure of state.
    # @!attribute name
    #   @return [Symbol] name of state.
    # @!attribute type
    #   @return [Symbol] type of state, which can be one of three predefined values.
    State = Struct.new(:name, :type)

    attr_accessor :alphabet, :states, :transitions
    attr_accessor :initial_state, :final_state

    # @api private
    def initialize
      self.alphabet = []
      self.states = []
      self.transitions = Set.new
    end

    # Process tape like the Turing Machine.
    # @api public
    # @param tape_string [String] string representation of tape.
    # @return [String] result of evaluating machine with this {Tape}.
    # @raise [Error] if the machine wasn't configured properly.
    # @example Read tape
    #   machine.read_tape!("abbbca") => "ccccca"
    # @example Raises Error
    #   machine.read_tape!("abb") => CoolTuringMachine::Error: Some error
    def read_tape!(tape_string)
      validate!

      tape = Tape.new(tape_string, initial_state.name)
      while tape.current_state != final_state.name
        transition = find_transition_by!(tape)
        tape.merge_state_from!(transition)

        error!("Negative cursor position") if tape.negative_position?
        error!("End of tape") if tape.end_of_tape?
      end

      tape.result
    end

    # Defines new alphabet for the machine.
    # @api public
    # @param alphabet_string [String] string, representing alphabet of the machine.
    # @return [Void]
    def define_alphabet(alphabet_string)
      self.alphabet = alphabet_string.split("").freeze
    end

    # Defines list of states. Overwrites previously described list.
    # @api public
    # @param state_names [Array<Symbol>] list of {State#name} values.
    # @return [Void]
    def define_states(*state_names)
      self.states = state_names.uniq.map { |name| State.new(name.to_sym, :normal) }.freeze
      states.each { |state| define_method_for_state(state) }
    end

    # Defines initial state by finding already defined state and changing state's type to :initial.
    # @api public
    # @param state_name [Symbol] name of existing state.
    # @raise [Error] if state not found.
    def define_initial_state(state_name)
      define_common_state!(:initial_state, state_name)
    end

    # Defines final state by finding already defined state and changing state's type to :final.
    # @api public
    # @param state_name [Symbol] name of existing state.
    # @raise [Error] if state not found.
    def define_final_state(state_name)
      define_common_state!(:final_state, state_name)
    end

    private

    def validate!
      if [alphabet, states].any?(&:empty?) || [initial_state, final_state].any?(&:nil?)
        error!("Need to properly define machine")
      end

      error!("Some transitions not fully declared") unless transitions.all?(&:valid?)
    end

    def state_by_name(state_name)
      states.find { |state| state.name == state_name.to_sym }
    end

    def define_method_for_state(state)
      define_singleton_method(state.name) { Transition.new(state.name, self) }
    end

    def define_common_state!(common_state_name, state_name)
      instance_variable_name = "@#{common_state_name}"
      return if instance_variable_defined?(instance_variable_name)

      humanized_state_name = common_state_name.to_s.split("_").map(&:capitalize).join(" ")
      found_state = state_by_name(state_name) or error!("#{humanized_state_name} not found")
      found_state.type = common_state_name.to_s.split("_").first

      instance_variable_set(instance_variable_name, found_state)
    end

    def find_transition_by!(tape)
      transition = transitions.find { |transition| tape.transition_matched?(transition) }

      transition or error!("Missed transition for #{tape}")
    end
  end
end
