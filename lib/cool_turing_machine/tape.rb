# frozen_string_literal: true

module CoolTuringMachine
  # Holds dynamic state of working machine
  # @todo Refactor this abstraction
  # @api private
  class Tape
    # @return [Symbol] name of active state
    attr_accessor :current_state

    # @return [String] string representation of tape
    attr_reader :tape_string

    # @return [Integer] position of tape's pointer
    attr_reader :current_cursor_position

    # Creates new instance of {Tape}
    # @param tape_string [String] tape in string format
    # @param initial_state [Symbol] name of active state.
    # @return [Tape] instance of {Tape}
    def initialize(tape_string, initial_state)
      @tape_string = tape_string.dup
      self.current_state = initial_state
      @current_cursor_position = 0
    end

    # @return [String] one character at {current_cursor_position} from tape
    def current_char
      @tape_string[@current_cursor_position]
    end

    # Change string character at {current_cursor_position}
    # @param value [String] changed character
    # @return [Void]
    def current_char=(value)
      @tape_string[@current_cursor_position] = value
    end

    # @see tape_string
    def result
      @tape_string
    end

    # Check for negative value of {#current_cursor_position}
    # @return [Boolean]
    def negative_position?
      @current_cursor_position.negative?
    end

    # @return [Boolean]
    def end_of_tape?
      @current_cursor_position >= @tape_string.length
    end

    # @return [Boolean] equality of transition attributes
    def transition_matched?(transition)
      current_state == transition.state && current_char == transition.matching_char
    end

    # Merge {Transition} states to self
    # @return [Void]
    def merge_state_from!(transition)
      self.current_char = transition.changing_char
      @current_cursor_position += transition.move_cursor_value
      self.current_state = transition.next_state
    end

    # @return [String]
    def to_s
      "#{current_state}-#{current_char}"
    end
  end
end
