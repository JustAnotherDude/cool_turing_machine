# frozen_string_literal: true

module CoolTuringMachine
  # Base class with commonly used methods.
  # @abstract
  class BaseObject
    private

    def error!(message)
      raise Error, message
    end
  end
end
