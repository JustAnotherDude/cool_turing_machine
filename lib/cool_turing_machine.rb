# frozen_string_literal: true

require "zeitwerk"

loader = Zeitwerk::Loader.for_gem
loader.setup

# Toplevel module of library. Provides a method for creating
# and configuring {Machine Turing Machine} instances.
# @see https://en.wikipedia.org/wiki/Turing_machine
module CoolTuringMachine
  # Custom Error
  # @api private
  class Error < StandardError; end

  # Creates a new instance of the state machine. Can accepts block
  # @api public
  # @yield [void] Configures new instance of the {Machine} class
  # @return [Machine] new instance of the Turing Machine
  # @example Create new Machine
  #   CoolTuringMachine.create { define_states :q0, :q1, :q2 }
  def self.create(&block)
    Machine.new.tap { |machine| machine.instance_eval(&block) if block_given? }
  end
end

loader.eager_load
