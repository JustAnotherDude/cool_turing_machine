# frozen_string_literal: true

describe CoolTuringMachine::Machine do
  subject(:machine) do
    described_class.new.tap do |machine|
      machine.define_alphabet(alphabet)
      machine.define_states(*state_names)

      machine.define_initial_state(state_names.first)
      machine.define_final_state(state_names.last)
      machine.q0
    end
  end

  let(:alphabet) { "123" }
  let(:state_names) { %i[q0 q1] }
  let(:common_error_message) { "Need to properly define machine" }

  context "without setted initial state" do
    subject(:machine) do
      described_class.new.tap do |machine|
        machine.define_alphabet(alphabet)
        machine.define_states(*state_names)

        machine.define_final_state(state_names.last)
        machine.q0.match("1")
      end
    end

    it "raises error" do
      expect { machine.read_tape!("123") }.to raise_error(common_error_message)
    end
  end

  context "without setted final state" do
    subject(:machine) do
      described_class.new.tap do |machine|
        machine.define_alphabet(alphabet)
        machine.define_states(*state_names)

        machine.define_initial_state(state_names.first)
      end
    end

    it "raises error" do
      expect { machine.read_tape!("123") }.to raise_error(common_error_message)
    end
  end

  context "without fully declared transition" do
    before { machine.q0.match("1").change_char_to("1").move(:right).change_state_to(:q0) }
    before { machine.q1.match("1") }

    it "raises error on #read_tape! call" do
      expect { machine.read_tape!("123") }.to raise_error("Some transitions not fully declared")
    end
  end

  context "with double definition of final state" do
    let(:state_names) { %i[q0 q1 q2] }

    it "doesn't change final state second time" do
      machine.define_final_state(:q1)

      expect(machine.final_state.name).to eq(:q2)
    end
  end

  context "with empty alphabet" do
    subject(:machine) do
      described_class.new.tap do |machine|
        machine.define_states(*state_names)

        machine.define_initial_state(state_names.first)
        machine.define_final_state(state_names.last)
      end
    end

    it "raises error" do
      expect { machine.read_tape!("123") }.to raise_error(common_error_message)
    end
  end

  context "with creating the same transition" do
    subject(:machine) do
      described_class.new.tap do |machine|
        machine.define_alphabet(alphabet)
        machine.define_states(*state_names)

        machine.define_initial_state(state_names.first)
        machine.define_final_state(state_names.last)
      end
    end

    it "inserts only first transition" do
      5.times { machine.q0.match("1") }
      expect(machine.transitions.size).to eq(1)
    end
  end

  context "with existed transition for the same state and character" do
    subject(:machine) do
      described_class.new.tap do |machine|
        machine.define_alphabet(alphabet)
        machine.define_states(*state_names)

        machine.define_initial_state(state_names.first)
        machine.define_final_state(state_names.last)

        machine.q0.match("1").stay_same_state.move(:nowhere).change_char_to("1")
        machine.q0.match("1").stay_same_state.move(:right).change_char_to("2")
      end
    end

    it "redefines previously defined transition" do
      expect(machine.transitions.size).to eq(1)
      expect(machine.transitions.to_a.last.changing_char).to eq("2")
    end
  end
end
