# frozen_string_literal: true

describe CoolTuringMachine::Transition do
  def setup_transition!(prepared_transition = transition)
    prepared_transition.match(match_char).change_char_to(change_char)
                      .move(move_side).change_state_to(change_state)
  end

  def expect_error!(message)
    expect { setup_transition! }.to raise_error(CoolTuringMachine::Error, message)
  end

  subject(:transition) { described_class.new(:q0, machine) }

  let(:match_char) { "a" }
  let(:change_char) { "b" }
  let(:move_side) { :right }
  let(:change_state) { :q1 }

  let(:machine) do
    instance_double(CoolTuringMachine::Machine, machine_stubs)
  end
  let(:machine_stubs) do
    { alphabet: machine_alphabet, states: machine_states, transitions: Set[] }
  end

  let(:machine_alphabet) { %w[a b c] }
  let(:machine_states) { [double(name: :q0, type: :normal), double(name: :q1, type: :normal)] }

  it "properly describes transition" do
    setup_transition!

    expect(transition).to have_attributes(
      state: :q0, matching_char: "a", changing_char: "b",
      move_cursor_value: 1, next_state: change_state, valid?: true
    )
  end

  context "with equal transitions" do
    let(:another_transition) { setup_transition!(described_class.new(:q0, machine)) }

    it "transitions properly compare with each other" do
      setup_transition!

      expect(transition.eql?(another_transition)).to eq(true)
      expect(transition.hash).to eq(another_transition.hash)
    end
  end

  context "with bad matching char" do
    let(:match_char) { "g" }

    specify { expect_error!("Alphabet hasn't 'g' char") }
  end

  context "with bad char, to which changing" do
    let(:change_char) { "g" }

    specify { expect_error!("Alphabet hasn't 'g' char") }
  end

  context "with partially defined transition" do
    it "returns false at valid? method" do
      expect(transition.valid?).to eq(false)
    end
  end

  context "with invalid move value" do
    let(:move_side) { :down }

    specify { expect_error!("Bad side") }
  end

  context "with invalid state, to which changing" do
    let(:change_state) { :q2 }

    specify { expect_error!("State not found") }
  end

  context "with transition, which doesn't change state" do
    it "uses new dsl method" do
      setup_transition!

      expect(transition.next_state).to eq(change_state)
      expect(transition.stay_same_state).to eq(transition)
      expect(transition.next_state).to eq(transition.state)
    end
  end

  context "with equality with some random object" do
    it "returns false" do
      expect(transition == Object.new).to eq(false)
    end
  end
end
