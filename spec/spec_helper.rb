# frozen_string_literal: true

require "bundler/setup"
require "deep-cover"
require "pry"
require "pry-byebug"
require "cool_turing_machine"

RSpec.configure do |config|
  config.default_formatter = :progress
  config.expose_dsl_globally = true
  config.filter_run_when_matching :focus
  config.order = "random"
  config.profile_examples = 5
  config.threadsafe = false

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.expect_with :rspec do |expectations|
    expectations.syntax = :expect
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  Kernel.srand config.seed
end
