# frozen_string_literal: true

describe CoolTuringMachine do
  def setup_machine!(spec_context = self, &block)
    machine.instance_eval do
      define_alphabet spec_context.machine_alphabet
      define_states *spec_context.machine_states

      define_initial_state :q0
      define_final_state :q1

      next instance_eval(&block) if block_given?

      q0.match("a").change_char_to("c").move(:right).change_state_to(:q0)
      q0.match("b").change_char_to("c").move(:right).change_state_to(:q0)
      q0.match("c").change_char_to("c").move(:nowhere).change_state_to(:q1)
    end
  end

  subject(:machine) { described_class.create }

  let(:machine_alphabet) { "abc" }
  let(:machine_states) { %i[q0 q1] }

  it "creates empty machine" do
    expect(machine).to have_attributes(alphabet: [], states: [], transitions: Set[])
  end

  specify { expect(described_class::VERSION).not_to eq(nil) }

  context "with empty block passed to create method" do
    subject(:machine) { described_class.create {} }

    it "creates new instance of Machine" do
      expect(machine.class).to eq(described_class::Machine)
    end
  end

  context "with proper machine" do
    before { setup_machine! }

    it "properly reads tape" do
      expect(machine.read_tape!("abbbca")).to eq("ccccca")
    end
  end

  context "without transition of needed tape char" do
    before do
      setup_machine! do
        q0.match("a").change_char_to("c").move(:right).change_state_to(:q0)
        q0.match("b").change_char_to("c").move(:right).change_state_to(:q0)
      end
    end

    specify { expect { machine.read_tape!("abbbca") }.to raise_error("Missed transition for q0-c") }
  end

  context "without transition of needed state" do
    before do
      setup_machine! do
        q0.match("a").change_char_to("c").move(:right).change_state_to(:q0)
        q0.match("b").change_char_to("c").move(:right).change_state_to(:q0)
        q0.match("c").change_char_to("c").move(:right).change_state_to(:q2)
      end
    end
    let(:machine_states) { %i[q0 q1 q2] }

    specify do
      expect { machine.read_tape!("abbbca") }.to raise_error("Missed transition for q2-a")
    end
  end

  context "with defined alphabet and states" do
    before { setup_machine! {} }

    it "properly creates" do
      expect(machine).to have_attributes(alphabet: %w[a b c], transitions: Set[])
      expect(machine.states.map(&:name)).to eq(%i[q0 q1])
    end
  end

  context "with not existed initial state" do
    let(:machine_states) { %i[q1 q2] }

    specify { expect { setup_machine! }.to raise_error("Initial State not found") }
  end

  context "with not existed final state" do
    let(:machine_states) { %i[q0 q2] }

    specify { expect { setup_machine! }.to raise_error("Final State not found") }
  end

  context "with negative cursor position" do
    before { setup_machine! { q0.match("a").change_char_to("a").move(:left).change_state_to(:q0) } }

    specify { expect { machine.read_tape!("aaaa") }.to raise_error("Negative cursor position") }
  end

  context "with reaching end of tape" do
    before do
      setup_machine! { q0.match("a").change_char_to("a").move(:right).change_state_to(:q0) }
    end

    specify { expect { machine.read_tape!("aa") }.to raise_error("End of tape") }
  end
end
